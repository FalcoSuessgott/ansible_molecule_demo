FROM docker:19.03.10

RUN apk add --no-cache \
    python3-dev \
    python \
    py3-pip \
    gcc \
    curl \
    build-base \
    autoconf \
    automake \
    py3-cryptography \
    linux-headers \
    musl-dev \
    libffi-dev \
    openssl-dev \
    openssh \
    git

RUN python3 -m pip install ansible \
    molecule \
    docker \
    yamllint \
    anybadge \
    testinfra \
    pytest \
    flake8 \
    ansible-lint 
