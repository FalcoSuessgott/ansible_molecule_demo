import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


# tests if apache user has been created
def test_apache_user(host):
    distro = host.system_info.distribution
    apache_user = "www-data" if distro == "debian" or distro == "ubuntu" else "apache"
    user = host.user(apache_user)

    assert user.name == apache_user
    assert user.group == apache_user


# tests if apache is running
def test_apache_is_running(host):
    distro = host.system_info.distribution
    apache_daemon = "apache2" if distro == "debian" or distro == "ubuntu" else "httpd"

    service = host.service(apache_daemon)
    assert service.is_running


# tests if port 80 is listening
def test_port_is_listening(host):
    assert host.socket("tcp://0.0.0.0:80").is_listening


# tests if localhost:80 returns HTTP200
def test_http_ok(host):
    status = host.run('curl -sL -w "%{http_code}" -I "127.0.0.1:80" -o /dev/null')
    assert status.stdout == "200"
