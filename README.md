# ansible_molecule_demo

Ansible Role for installing and configuring apache on :

| OS | ![centos8](https://gitlab.com/FalcoSuessgott/ansible_molecule_demo/-/jobs/artifacts/master/raw/public/centos8.svg?job=centos8) |![centos8](https://gitlab.com/FalcoSuessgott/ansible_molecule_demo/-/jobs/artifacts/master/raw/public/centos8.svg?job=centos8) |![centos7](https://gitlab.com/FalcoSuessgott/ansible_molecule_demo/-/jobs/artifacts/master/raw/public/centos7.svg?job=centos7) | ![ubuntu1804](https://gitlab.com/FalcoSuessgott/ansible_molecule_demo/-/jobs/artifacts/master/raw/public/ubuntu1804.svg?job=ubuntu1804) |![ubuntu2004](https://gitlab.com/FalcoSuessgott/ansible_molecule_demo/-/jobs/artifacts/master/raw/public/ubuntu2004.svg?job=ubuntu2004) | ![debian10](https://gitlab.com/FalcoSuessgott/ansible_molecule_demo/-/jobs/artifacts/master/raw/public/debian10.svg?job=debian10) | 
| --- | --- | --- | --- | --- | --- | --- | 



## Description
Demo Project for testing ansible with molecule for ansible-anwender meeting 29.10.2020 (https://www.ansible-anwender.de/2020/10/06/__trashed/)

## Installation
```
python3 -m pip install "molecule[ansible]"
```

## Usage

```bash
git clone https://gitlab.com/falcosuessgott/ansible_molecule_demo
cd ansible_molecule_demo
molecule test
```